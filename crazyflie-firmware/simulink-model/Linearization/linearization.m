clc, clearvars
syms m g b k d Jx Jy Jz real
angle = sym('angle', [3,1], 'real');
dangle = sym('dangle', [3,1], 'real');
p = sym('p', [3,1], 'real');
v = sym('v', [3,1], 'real');
tau = sym('tau', [3,1], 'real');
ctrl = sym('ctrl', [4,1], 'real');

J = [Jx,0,0;0,Jy,0;0,0,Jz];


x = [p; angle; v; dangle];

% R = [cos(angle(3)) -sin(angle(3)) 0;sin(angle(3)) cos(angle(3)) 0;0 0 1] * ...
%     [cos(angle(2)) 0 sin(angle(2));0 1 0;-sin(angle(2)) 0 cos(angle(2))] * ...
%     [1 0 0;0 cos(angle(1)) -sin(angle(1));0 sin(angle(1)) cos(angle(1))]; % Rz*Ry*Rx
R = [1 0 0;0 cos(angle(1)) -sin(angle(1));0 sin(angle(1)) cos(angle(1))] * ...
    [cos(angle(2)) 0 sin(angle(2));0 1 0;-sin(angle(2)) 0 cos(angle(2))] * ...
    [cos(angle(3)) -sin(angle(3)) 0;sin(angle(3)) cos(angle(3)) 0;0 0 1]; % Rx*Ry*Rz

w_motor = sqrt(ctrl/b);
Q = k*w_motor.^2;
tau(3) = -Q(1) + Q(2) - Q(3) + Q(4);
tau(1) = d/sqrt(2)*(-ctrl(1) + ctrl(4) - ctrl(2) + ctrl(3));
tau(2) = d/sqrt(2)*(-ctrl(1) + ctrl(2) + ctrl(3) - ctrl(4));

f([ctrl; x]) = simplify([v
    dangle
    1/m*([0; 0; -m*g] + R * [0; 0; sum(ctrl)])
    J\(-cross(dangle, J*dangle) + tau)]);

df_dx([ctrl; x]) = jacobian(f, x);
Ac = df_dx(m*g/4, m*g/4, m*g/4, m*g/4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
Ac_sym = Ac;

df_du([ctrl; x]) = jacobian(f, ctrl);
Bc = df_du(m*g/4, m*g/4, m*g/4, m*g/4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
Bc_sym = Bc;

Cc = [Ac(7:9,:)
    0 0 0 0 0 0 0 0 0 1 0 0
    0 0 0 0 0 0 0 0 0 0 1 0
    0 0 0 0 0 0 0 0 0 0 0 1];
Cc_sym = Cc;
Dc = [Bc(7:9, :)
    zeros(3, 4)];
Dc_sym = Dc;


matlabFunction(Ac, Bc, Cc, Dc, 'file', 'Linearization_matrices', 'vars', {m, g, b, k, d, Jx, Jy, Jz})

Jx = 1.395e-5;
Jy = 1.436e-5;
Jz = 2.173e-5;
m = 0.027;
g = 9.81;
d = 0.046;
k = 1e-9;
b = 2.75e-11;

[Ac, Bc, Cc, Dc] = Linearization_matrices(m, g, b, k, d, Jx, Jy, Jz)


    